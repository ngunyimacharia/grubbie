<?php include('../includes/header.php'); ?>
    <h1>Rate your meals</h1>

    <!-- days of the week -->
    <?php include('../includes/ui/days.php'); ?>
    <!-- food rating -->
    <div class="rounded-container">
        <div class="rate-wrapper">
            <div class="rounded-image"></div>
            <div class="rate-text">
                Tea and Bread <br/>
                <?php include('../includes/ui/stars.php'); ?>
                <input type="text" name="" id="">
            </div>
        </div>
    </div>

    <div class="rounded-container">
        <div class="rate-wrapper">
            <div class="rounded-image"></div>
            <div class="rate-text">
                Jollof Rice <br/>
                <?php include('../includes/ui/stars.php'); ?>
                <input type="text" name="" id="">
            </div>
        </div>
    </div>
    
    <div class="rounded-container">
        <div class="rate-wrapper">
            <div class="rounded-image"></div>
            <div class="rate-text">
                Rice and Stew <br/>
                <?php include('../includes/ui/stars.php'); ?>
                <input type="text" name="" id="">
            </div>
        </div>
    </div>
    
<?php include('../includes/footer.php'); ?>
